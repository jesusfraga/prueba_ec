from django.apps import AppConfig


class EnvioclickAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'envioclick_app'
