from django.db import models

# Create your models here.

class VehiculoMarca(models.Model):
    nombre = models.CharField(max_length=100)    

class VehiculoModelo(models.Model):
    marca_id = models.ForeignKey(VehiculoMarca, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)
    año = models.IntegerField()   

class Vehiculo(models.Model):
    modelo = models.ForeignKey(VehiculoModelo, on_delete=models.CASCADE)
    tipo = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200)

class Persona(models.Model):
    nombre = models.CharField(max_length=100)
    apellido_paterno = models.CharField(max_length=100)
    apellido_materno = models.CharField(max_length=100)

class VehiculoPersona(models.Model):
    persona_id = models.ManyToManyField(Persona)  
    vehiculo_id = models.ManyToManyField(Vehiculo) 

class AsignacionManejoVehiculo(models.Model):    
    persona_id = models.ForeignKey(Persona, on_delete=models.CASCADE)
    vehiculo_id = models.ForeignKey(Vehiculo, on_delete=models.CASCADE)
    fecha_expiracion = models.DateTimeField()

